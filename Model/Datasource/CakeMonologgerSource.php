<?php 

App::uses('Mysql', 'Model/Datasource/Database');


class CakeMonologgerSource extends Mysql {

	public function delete(Model $Model, $conditions = null) {
		parent::delete($Model,$conditions);
		$this->_log();
	}

	public function create(Model $Model, $fields = NULL, $values = NULL) {
		parent::create($Model,$fields,$values);
		$this->_log();
	}

	public function update(Model $model, $fields = array(), $values = NULL, $conditions = NULL){
		parent::update($model,$fields,$values,$conditions);
		$this->_log();
	}

	public function fetchAll($sql, $params = array(), $options = array()) {
		
		$this->_result = parent::fetchAll($sql,$params,$options);

		if( strpos($sql,'SELECT COUNT(*) AS `count` FROM') === false ) {
			$this->_log();
		}

		return $this->_result;

	}

	private function _trace(){
		
		$pos = max(count($this->_queriesLog) - 1,0);
		
		return array(
			'log' => CakeMonologger::trace(),
			'sql' => $this->_queriesLog[$pos]  
		);

	}

	private function _log(){
		/* array( query,params,affected,numRows,took ) */
		$trace = self::_trace();
		$pos = count($trace['log'])-6;
		
		CakeMonologger::log( 'debug', 'start', $trace['log'][$pos], 1 );
		CakeMonologger::log( 'debug', 'param', json_encode($trace['sql']['params']) );
		CakeMonologger::log( 'debug', 'sql', $trace['sql']['query'] );
		
		if( ! isset($this->_result->queryString) ) {
			$result = json_encode($this->_result);
			$length = strlen($result);
			/* select result */
			CakeMonologger::log( 'debug', 'result', $result);
			
		}
		
		CakeMonologger::log( 'debug', 'result', 
			'Affected: ' . $trace['sql']['affected'] . '; ' .
			'Num rows: ' . $trace['sql']['numRows'] . '; ' .
			'Took: ' . $trace['sql']['took'] . 'ms;'
		);
		
		CakeMonologger::log( 'info', 'end', $trace['log'][$pos], 0 );
		
	}
	
}

