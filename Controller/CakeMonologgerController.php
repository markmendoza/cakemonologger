<?php 

$vendor_path1 = '../../vendor/autoload.php';
$vendor_path2 = '../../vendors/autoload.php';

if( file_exists($vendor_path1) ){
	require_once $vendor_path1;
}else if( file_exists($vendor_path2) ){
	require_once $vendor_path2;
}else{
	if( Configure::read('debug') > 0 )
		exit('Missing /vendor in base directory. <br/><br/>See <a href="https://bitbucket.org/markmendoza/cakemonologger" target="_blank">https://bitbucket.org/markmendoza/cakemonologger</a>');
}


use Monolog\Logger;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\RotatingFileHandler;

App::uses('CakeMonologger','CakeMonologger.Controller');


class CakeMonologgerController extends Controller{
	private $_request;
	public function beforeFilter(){
		
		$this->_request = $this->request->params['controller'] .'/'. $this->request->params['action'];

		CakeMonologger::init();
		CakeMonologger::log( 'info', 'start',  $this->_request, 1 );

		if(count($this->request->params) > 0)
			CakeMonologger::log( 'info', 'input', '$this->request->params = '. json_encode($this->request->params) );
		if(count($this->request->data) > 0)
			CakeMonologger::log( 'notice', 'input', '$this->request->data = '. json_encode($this->request->data) );
		if(count($this->request->query) > 0)
			CakeMonologger::log( 'info', 'input', '$this->request->query = '. json_encode($this->request->query) );
	
	}

	public function afterFilter(){
		CakeMonologger::log( 'info', 'end', $this->_request, 0 );
		
		if( ! defined('CAKEMONOLOGGER_DB_SOURCE') ){
			CakeMonologger::log( 'notice', 'CONFIG', 'You need to modify the datasource in app/Config/database.php to log query info. See #6 in https://bitbucket.org/markmendoza/cakemonologger' );
		}
		if( CakeMonologger::enabled() &&  ! error_get_last() ){
            ob_end_flush();
            flush();
        }
	}
	
}
