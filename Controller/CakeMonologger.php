<?php 


use Monolog\Logger;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\RotatingFileHandler;


class CakeMonologger{
    
	private static $_instance = false;
	private static $_browserConsoleHandler = false;
	private static $_formatter = array();

	public static $format = '[[%level_name%]]{ background: {{$color}}; padding:2px;color:white; margin-right:10px; font-weight:bold; border-radius: 3px }%message%';
	public static $background = array(
		'INFO' => '#008000',
		'NOTICE' => '#FFA500',
		'WARNING' => '#FFA500',
		'ERROR' => '#971419',
		'SQL' => '#055A89',
		'DEBUG' => '#055A89',
		'CRITICAL' => '#971419',
		'ALERT' => '#971419',
		'EMERGENCY' => '#971419'
	);

    public static function enabled(){
        return CAKEMONOLOGGER_ALLOW_IN_DEVELOPMENT || CAKEMONOLOGGER_ALLOW_IN_PRODUCTION;
    }

    public static function file_logging(){
        return self::enabled();
    }    
    

    /* Don't allow console logging in production */
    public static function console_logging(){
        return CAKEMONOLOGGER_ALLOW_IN_DEVELOPMENT;
    }

	private static function setFormatter($level = 'INFO'){
		if( ! isset(self::$_formatter[$level]) ){
			self::$_formatter[$level] = new LineFormatter( str_replace(array('{{$color}}'), array(self::$background[$level]), self::$format) );
		}
       
        if( self::file_logging() ){
		    self::$_browserConsoleHandler->setFormatter( self::$_formatter[$level] );
        }
	}
    

	/* Create monolog instance */
	public static function init( ){
        if( ! self::enabled() ) return;
		if( ! self::$_instance ){
                
			self::$_instance = new Monolog\Logger('');
		
        
            $rotatingFileHandler = new RotatingFileHandler('../Plugin/CakeMonologger/Logs/app.info');
            $rotatingFileHandler->setFormatter( new LineFormatter("%level_name%\t%message%\n") );

            /* Always create log files */
            self::$_instance->pushHandler( $rotatingFileHandler );

            if( self::console_logging() ){ 
                /* monolog browserconsole */
                self::$_browserConsoleHandler = new BrowserConsoleHandler();
                self::$_instance->pushHandler( self::$_browserConsoleHandler );

                /* php-console plugin */
                $handler = PhpConsole\Handler::getInstance();
                $handler->start();
                $handler->getConnector()->setSourcesBasePath($_SERVER['DOCUMENT_ROOT']);
            }
        }
	}

	private static function getTimestamp(){
		return date(DateTime::ATOM);
	}

    private static $_timer = array();
    
	public static function log($level = 'INFO',$description = NULL, $message=NULL, $timer = null){

        if( ! self::enabled() ) return;

		$_level = array('u' => strtoupper($level));
      
       $description = strtoupper($description);
       $time = microtime(true); 
       
       /* End */
       if(isset($timer) && $timer == 0){
           $start_time = array_pop(self::$_timer);

           CakeMonologger::log( 'info', 'end', json_encode(array(
                'start' => $start_time,
                'end' => $time,
                'diff' => $time - $start_time,
            )) );
       }
       
        if( $message && self::$_instance && self::$background[$_level["u"]] ){
            
            self::setFormatter($_level["u"]);
            self::$_instance->log( $level, self::getTimestamp() ."\t". $description . "\t" . $message );
        }

        /* Start */
       if($timer && $timer == 1){
           self::$_timer[] = $time;
           CakeMonologger::log( 'info', 'start', $time );
       }
    
       
	}

    public static function trace(){
        return explode("\n",Debugger::trace());
    }

}
