<?php 


App::uses('CakeMonologger','CakeMonologger.Controller');

class CakeMonologgerHandler extends ErrorHandler{ 
    
	public static function ErrorHandler($code, $description, $file = null, $line = null, $context = null){
        CakeMonologger::log( parent::mapErrorCode($code)[0], 'message' , $description . ' in ' . $file . ' at Line ' . $line );
	}

	public static function ExceptionHandler($e){
        CakeMonologger::log( 'critical', 'message' ,  $e->getMessage() . ' in ' . $e->getFile() . ' at Line ' . $e->getLine() );
	}
    
}