# Requirements

*Cakephp 2+*


# Installation



*1. Configure Cakephp framework, add database connection,change salt string etc*


*2. Copy CakeMonologger plugin in app/Plugin directory*


```

/app/Plugin/CakeMonologger

```

*3. Install monolog and php console using composer*


```

composer require monolog/monolog:"1.21" php-console/php-console:"3.0"

```


```
/app
/lib
/vendors
    /monolog
    /php-console
    ....
composer.json

```



*4. Add the following code in app/Config/bootstrap.php*


```
/* Important: Unload/remove all debugkit components in controllers */

/* Set to true to enable both "file" and "console" logging in dev environment */
define('CAKEMONOLOGGER_ALLOW_IN_DEVELOPMENT', Configure::read('debug') > 0 ? true : false);

/* Set to true to allow "file" logging in production */
define('CAKEMONOLOGGER_ALLOW_IN_PRODUCTION', false);


CakePlugin::load('CakeMonologger');

Configure::write('Error.handler', 'CakeMonologgerHandler::ErrorHandler');
Configure::write('Exception.handler', 'CakeMonologgerHandler::ExceptionHandler');


```



*5. Add the following code in app/Config/core.php*


```
/* Optional */
define( 'IN_LOCAL_ENV' , (strpos('192.168',$_SERVER['SERVER_ADDR']) < 1 or  strpos('::1',$_SERVER['SERVER_ADDR']) < 1) );

/* Set debug, modify to configure for different environments */
Configure::write('debug', IN_LOCAL_ENV ? 2 : 0 );



App::uses('CakeMonologgerHandler','CakeMonologger.Error');

```



*6. Make the following modifications in app/Config/database.php*



```

/* Add constant */
define('CAKEMONOLOGGER_DB_SOURCE', 'CakeMonologger.CakeMonologgerSource'); 

/* Modify datasource */
class DATABASE_CONFIG {

	public $default = array(
		'datasource' => CAKEMONOLOGGER_DB_SOURCE,

	...
	...
	
```


*7. Open app/Controller/AppController.php and make the following modifications*


a. Replace App::uses('Controller','Controller'); with 

```	
App::uses('CakeMonologgerController', 'CakeMonologger.Controller');

```

b. then extend CakeMonologgerController

```

App::uses('CakeMonologgerController', 'CakeMonologger.Controller');

class AppController extends CakeMonologgerController {
	public function beforeFilter(){
		parent::beforeFilter();
		/* your code here */
	}

	public function afterFilter(){
		/* your code here */
		parent::afterFilter();
	}
}

```


*8. Launch cake page, you should see logs in console(F12) and logs in /app/Plugin/CakeMonologger/Logs folder*
	

